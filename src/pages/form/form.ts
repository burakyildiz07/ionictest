import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



/**
 * Generated class for the FormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    template: `
        <ion-header>
      <ion-navbar>
        <ion-title>
          Form
        </ion-title>
      </ion-navbar>
    </ion-header>
    
    <ion-content padding>
        <form (ngSubmit)="logForm()">
            <ion-item>
                <ion-label>Ad </ion-label>
                <ion-input type="text" [(ngModel)]="data.name" name="title"></ion-input>
            </ion-item>
            <ion-item>
                <ion-label>Soyad </ion-label>
                <ion-input type="text" [(ngModel)]="data.surname" name="surname"></ion-input>
            </ion-item>
            <ion-item>
                <ion-label>Tc Kimlik </ion-label>
                <ion-input type="text" [(ngModel)]="data.identificationKey" name="identificationKey"></ion-input>
            </ion-item>
            <ion-item>
                <ion-label>Mesaj</ion-label>
                <ion-textarea [(ngModel)]="data.message" name="message"></ion-textarea>
            </ion-item>
            <button ion-button type="submit" block>Gönder</button>
        </form>
    </ion-content>
  `,
})
export class FormPage {
    data = {};

    constructor(public navCtrl: NavController, public navParams: NavParams) {


    }
    logForm() {

    }
}