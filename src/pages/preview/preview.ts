import { Component } from '@angular/core';
import {
    ModalController,
    IonicPage,
    NavController,
    NavParams,
    AlertController,
    Platform,
    LoadingController
} from 'ionic-angular';
import {PreviewModalPage} from "../preview-modal/preview-modal";
import { HTTP } from '@ionic-native/http';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';



/**
 * Generated class for the PreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import {HomePage} from '../home/home';
import {InstitutionsPage} from "../institutions/institutions";

@IonicPage()
@Component({
  selector: 'page-preview',
  templateUrl: 'preview.html',
})
export class PreviewPage {

  base64=[];
  image:string;
  loading;
  data = {
    message:''
  }
   lat: number;
   long: number;
  constructor(private platform: Platform,public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,
              public loadingCtrl: LoadingController,private storage: Storage,private http: HTTP,private alertCtrl: AlertController,private geolocation: Geolocation) {
  }

    ionViewWillEnter() {
        var element = document.getElementById("ion-app");
        element.className = element.className.replace(/\bmystyle\b/g, "");
        let app = this;
        app.base64 = app.navParams.get('base64');
        app.image = app.base64[0];

        app.geolocation.getCurrentPosition().then((resp) => {
            app.lat = (resp.coords.latitude);
            app.long =(resp.coords.longitude);
        }).catch((error) => {
            console.log('Error getting location', error);
        });

        app.storage.get('lastSuccessIncident').then((val) => {

            if(val && val.length>1)
           {
               if(val[val.length-1]>((Date.now()/1000)-300) && val[val.length-2]>((Date.now()/1000)-300))
               {
                   let alert = app.alertCtrl.create({
                       title: 'Zaman Aşımı',
                       subTitle: 'Tekrar gönderimde bulunmak için biraz beklemelisiniz.',
                       buttons: [
                           {
                               text: 'Tamam',
                               handler: () => {
                                   app.navCtrl.popToRoot();
                               }
                           },
                       ]
                   });
                   alert.present();
               }
           }
        });


    }
    postForm()
    {
        let app = this;

        let alert = this.alertCtrl.create({
            title: 'İşlem Onayı',
            cssClass:'buttonCss',
            buttons: [
                {
                    text: 'Başvuru Oluştur',
                    handler: () => {
                       app.postImage(app.image);
                    },
                },
                {
                    text: 'İptal',
                    role: 'cancel',
                }
            ]
        });
        alert.present();

    }

    incidentPost(text,files)
    {
        var url = 'https://webservis.ulakbel.com/common/mobile.php';
        let app =this;
        let geogrophic_coordinate={
            lat:null,
            long:null,
        };

        if(app.long>0 && app.lat>0)
        {
            geogrophic_coordinate.lat = app.lat;
            geogrophic_coordinate.long= app.long;
        }
        let json = {text:text,attachments:files,geogrophic_coordinate:geogrophic_coordinate};
        app.http.post(url, {
            json:JSON.stringify(json),
            function:'createIncident',
            token:'QUytvDlkCKCsYXPlOA3g0ayi4SuWwpvVZRJcw7DJVxugk0x05Lo9687eaTdi06w',
        }, {})
            .then(data => {
                app.loading.dismiss();

                let result = JSON.parse(data.data);
                let alert = app.alertCtrl.create({
                    title: 'İşlem Başarılı',
                    subTitle: 'Başvurunuz başarıyla oluşturulmuştur.' +
                    'Başvuru numaranız:'+result.number,
                    buttons: [
                        {
                            text: 'Tamam',
                            handler: () => {
                                app.navCtrl.popToRoot();
                            }
                        },
                    ]
                });
                alert.present();

            })
            .catch(error => {
                app.loading.dismiss();

                console.log(error.status);
                console.log(error.error); // error message as string
                console.log(error.headers);

                return false;
            });
    }

    preview()
    {
        this.navCtrl.pop();
    }

    postImage(base64)
    {
        let app = this;
        var url = 'https://webservis.ulakbel.com/common/mobile.php';

         app.loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: 'Başvurunuz oluşturuluyor.'
        });

        app.loading.present();


        app.http.post(url, {
            base64:base64,
            function:'storeBase64',
            token:'QUytvDlkCKCsYXPlOA3g0ayi4SuWwpvVZRJcw7DJVxugk0x05Lo9687eaTdi06w',
        }, {})
            .then(data => {
                let files =[];
                files.push(JSON.parse(data.data).files[0]);
                app.incidentPost(app.data.message,files);

                app.storage.get('lastSuccessIncident').then((val) => {
                    if(val)
                    {
                        val.push((Date.now()/1000));
                        app.storage.set('lastSuccessIncident',val);

                    }else{
                        app.storage.set('lastSuccessIncident', [(Date.now()/1000)]);
                    }
                });

            })
            .catch(error => {
                    app.loading.dismiss();

                console.log(error.status);
                console.log(error.error); // error message as string
                console.log(error.headers);
                return false;
            });
    }

    networkCheck()
    {
        var url = 'https://webservis.ulakbel.com/common/mobile.php';
        let app = this;
        app.http.post(url, {
            function:'ping',
            token:'QUytvDlkCKCsYXPlOA3g0ayi4SuWwpvVZRJcw7DJVxugk0x05Lo9687eaTdi06w',
        }, {})
            .then(data => {
                return true;
            })
            .catch(error => {
                let alert = app.alertCtrl.create({
                    title: 'İntenet Yok',
                    subTitle: 'Tekrar deneyin.',
                    buttons: [
                        {
                            text: 'Tamam',
                            handler: () => {
                                app.platform.exitApp()
                            }
                        },
                    ]
                });
                alert.present();
            });
    }
}
