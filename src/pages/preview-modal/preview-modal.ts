import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController  } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

/**
 * Generated class for the PreviewModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-preview-modal',
  templateUrl: 'preview-modal.html',
})
export class PreviewModalPage {

    incidentForm: FormGroup;
    constructor(public navCtrl: NavController, public viewCtrl : ViewController ,public navParams: NavParams,private fb: FormBuilder) {
        this.incidentForm = fb.group({
            'name' : [null, Validators.compose([Validators.required, Validators.maxLength(50)])],
            'surname' : [null, Validators.compose([Validators.required, Validators.maxLength(50)])],
            'identificationKey' : [null, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
            'numberPhone' : [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
        });
    }

    public closeModal(){
        this.viewCtrl.dismiss();
    }

    submitForm()
    {

    }

}
