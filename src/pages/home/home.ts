import { Component } from '@angular/core';
import { NavController,AlertController,Platform  } from 'ionic-angular';
import { CameraPreview } from '@ionic-native/camera-preview';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';
import { FileTransfer,  FileTransferObject } from '@ionic-native/file-transfer';
import { StatusBar } from '@ionic-native/status-bar';
import { Network } from '@ionic-native/network';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { File } from '@ionic-native/file';

import {InstitutionsPage} from '../institutions/institutions';
import {PreviewPage} from '../preview/preview';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    base64 = [];
    kurumList=[];
    image:string;
    process=false;
    private fileTransfer: FileTransferObject;
    constructor(private screenOrientation: ScreenOrientation,private platform: Platform,private alertCtrl: AlertController,private network: Network
                ,public navCtrl: NavController,private cameraPreview: CameraPreview,private http: HTTP,private storage: Storage,private transfer: FileTransfer, private file: File,private statusBar: StatusBar) {
        let app = this;
        app.statusBar.hide();
        app.screenOrientation.lock(app.screenOrientation.ORIENTATIONS.PORTRAIT);
        app.process = false;
    }
    ionViewWillEnter() {
        let app = this;
        app.networkCheck();
        try {
            try{
                setTimeout(function(){
                    app.stopCamera();
                }, 444);

            }catch(e)
            {

            }

            setTimeout(function(){ app.startCamera();
                app.process = false;
            }, 666);
        }
        catch(err) {
            setTimeout(function(){
                app.stopCamera();
            }, 444);
            setTimeout(function(){
                app.startCamera();
                app.process = false;
            }, 1000);
        }
    }

    public download(value) {

        let app = this;
        let url = encodeURI(value.icon);
        let imageName = value.icon.split('/');
        app.fileTransfer = this.transfer.create();
        app.fileTransfer.download(url, this.file.dataDirectory  + imageName[imageName.length-1], true).then((entry) => {
            app.kurumList.push({
                icon:value.icon,
                title:value.title,
                kurum_uid:value.kurum_uid,
                local_icon:entry.toURL() ,
            });
            app.storage.set('kurumList', app.kurumList);

        }, (error) => {
            console.log(error);
        });
    }


    getKurumList()
    {
        let app = this;
        var url = 'https://webservis.ulakbel.com/common/mobile.php';
        app.http.post(url, {
            token:'QUytvDlkCKCsYXPlOA3g0ayi4SuWwpvVZRJcw7DJVxugk0x05Lo9687eaTdi06w',
            function:'getKurumKoduList',
        }, {})
            .then(data => {
                let kurumlar = JSON.parse(data.data);
                kurumlar.forEach(function (value) {
                     app.download(value);
                })
            })
            .catch(error => {

                console.log(error.status);
                console.log(error.error); // error message as string
                console.log(error.headers);

            });
    }

    startCamera()
    {
        this.cameraPreview.startCamera({x: 0, y: 0, width: window.screen.width, height:window.screen.height, camera: "back", toBack: true, previewDrag: false, tapPhoto: false})
    }
    stopCamera()
    {
        this.cameraPreview.stopCamera();
    }

    takePhoto()
    {
        let app = this;
        app.base64=[];

        if(!app.process)
        {
            app.process = true;
            try {
                app.cameraPreview.takePicture({width:1280, height:720, quality: 70}).then((imageData) => {
                    try {
                        app.process = false;
                        app.base64.push('data:image/jpeg;base64,' + imageData);
                        app.cameraPreview.stopCamera();
                        app.navCtrl.push(PreviewPage,{base64:app.base64});
                    }
                    catch (e) {
                        app.process = false;
                    }
                });
            }catch (e) {
                app.process = false;
            }
        }

        setTimeout(function(){
            app.process = false;
        }, 1000);
    }
    nextPage()
    {
        let app = this;
        try {
            setTimeout(function(){
                app.cameraPreview.stopCamera();
            }, 444);
        }
        catch(err) {
            setTimeout(function(){
                app.cameraPreview.stopCamera();
            }, 1000);
        }
        this.navCtrl.push(InstitutionsPage);
    }

    networkCheck()
    {
        var url = 'https://webservis.ulakbel.com/common/mobile.php';
        let app = this;
        app.http.post(url, {
            function:'ping',
            token:'QUytvDlkCKCsYXPlOA3g0ayi4SuWwpvVZRJcw7DJVxugk0x05Lo9687eaTdi06w',
        }, {})
            .then(data => {
                return true;
            })
            .catch(error => {
                let alert = app.alertCtrl.create({
                    title: 'İntenet Yok',
                    subTitle: 'Tekrar deneyin.',
                    buttons: [
                        {
                            text: 'Tamam',
                            handler: () => {
                                app.platform.exitApp()
                            }
                        },
                    ]
                });
                alert.present();

            });
    }




}
