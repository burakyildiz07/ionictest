import { Component,ViewChild  } from '@angular/core';
import { IonicPage, NavController, Navbar ,NavParams } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
import { Storage } from '@ionic/storage';


import {FormPage} from '../form/form';

/**
 * Generated class for the InstitutionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-institutions',
  templateUrl: 'institutions.html',
})
export class InstitutionsPage {
    @ViewChild(Navbar) navbarName: Navbar;

    attachments=[];
   base64=[]    ;
  kurumList=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private http: HTTP,private storage: Storage) {


  }
    ionViewWillEnter() {
        var element = document.getElementById("ion-content");
        element.className = element.className.replace(/\bmystyle\b/g, "");
        var element = document.getElementById("ion-app");
        element.className = element.className.replace(/\bmystyle\b/g, "");
        let app = this;
        app.kurumList=[];
        app.base64=[];
        app.storage.get('base64').then((val) => {
            app.base64=val;
        });

        app.storage.get('kurumList').then((val) => {
            val.forEach(function (value) {

                value.local_icon = value.local_icon.replace('file://','http://localhost:8080/_file_/');
                app.kurumList.push(value);

            })
        });
    }

  postImage(kurum_uid,base64)
  {
      var url = 'https://webservis.ulakbel.com/common/mobile.php';

      this.http.post(url, {
          base64:base64,
          function:'storeBase64',
          kurum_uid:kurum_uid,
          token:'QUytvDlkCKCsYXPlOA3g0ayi4SuWwpvVZRJcw7DJVxugk0x05Lo9687eaTdi06w',
      }, {})
          .then(data => {
                console.log(data.data);
                this.attachments.push(JSON.parse(data.data).files[0]);
                this.storage.set('attachments', this.attachments);
          })
          .catch(error => {
              console.log(error.status);
              console.log(error.error); // error message as string
              console.log(error.headers);
          });
  }

  selectInstitutions(kurum_uid) {
      let app = this;
      app.attachments=[];
      app.base64.forEach(function (value) {
          app.postImage(kurum_uid,value.__zone_symbol__value[0])
      });
      app.navCtrl.push(FormPage);

  }

}
