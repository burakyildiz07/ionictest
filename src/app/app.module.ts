import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CameraPreview } from '@ionic-native/camera-preview';
import { HTTP } from '@ionic-native/http';
import { IonicStorageModule } from '@ionic/storage';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { File } from '@ionic-native/file';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { InstitutionsPage } from '../pages/institutions/institutions';
import { FormPage } from '../pages/form/form';
import { PreviewPage } from '../pages/preview/preview';
import { PreviewModalPage } from '../pages/preview-modal/preview-modal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FormPage,
    InstitutionsPage,
    PreviewPage,
    PreviewModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{ animate: false }),
    IonicStorageModule.forRoot()

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FormPage,
    InstitutionsPage,
    PreviewPage,
    PreviewModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CameraPreview,
    HTTP,
    ScreenOrientation,
    FileTransfer,
    FileTransferObject,
    HttpModule,
    Network,
    Geolocation,
    File,
      {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
